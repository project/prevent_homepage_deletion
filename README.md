# PREVENT HOMEPAGE DELETION

## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 -Maintainers

## INTRODUCTION

Prevent homepage deletion

This module provides a new permission: delete_homepage_node.
Only users with this permission can delete or unpublish the node that is
currently configured as the home page of the site. Other users will not
see the delete-tab, nor the delete option in the content overview. They will
also not see the published-checkbox when editing a page.

If they try to delete or unpublish the homepage node as part of a bulk-action
a message will be thrown why the page is not deleted.

Issues
<https://www.drupal.org/project/issues/prevent_homepage_deletion>

## REQUIREMENTS

No

## INSTALLATION

- Install with composer
$ composer require 'drupal/prevent_homepage_deletion:^1.0'
- Give someone the permission to delete the homepage, or don't if nobody should
delete it.

## CONFIGURATION

- Configure the user permissions in Administration » People » Permissions

## MAINTAINERS

Current maintainers:
 - Josha Hubbers (JoshaHubbers) - https://www.drupal.org/u/joshahubbers-0
