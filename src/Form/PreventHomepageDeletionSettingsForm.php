<?php

namespace Drupal\prevent_homepage_deletion\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PreventHomepageDeletionSettingsForm.
 *
 * The config form for the prevent_homepage_deletion module.
 *
 * @package Drupal\prevent_homepage_deletion\Form
 */
class PreventHomepageDeletionSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'prevent_homepage_deletion.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'prevent_homepage_deletion_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('prevent_homepage_deletion.settings');
    $form['protected_urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Protect these URL's"),
      '#description' => $this->t('Enter a list of page paths to protect against
      deletion. Start with "/". Wildcards are not supported.<br><strong>One item per line.</strong>'),
      '#default_value' => $config->get('protected_urls'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $this->config('prevent_homepage_deletion.settings')
      ->set('protected_urls', $form_state->getValue('protected_urls'))
      ->save();
  }

}
