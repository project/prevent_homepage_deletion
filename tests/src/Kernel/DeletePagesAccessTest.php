<?php

namespace Drupal\prevent_homepage_deletion\Tests\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Test for the delete pages permission.
 */
class DeletePagesAccessTest extends KernelTestBase {
  use UserCreationTrait;
  use NodeCreationTrait;
  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'system',
    'text',
    'user',
    'workspaces',
    'prevent_homepage_deletion',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('workspace');
    $this->installSchema('node', ['node_access']);
    $this->installSchema('workspaces', ['workspace_association']);
    $this->installConfig(['filter', 'node', 'system']);
    $this->createContentType(['type' => 'page']);

    // User with no permissions.
    $this->setUpCurrentUser([], [
      'access content',
      'create page content',
      'edit any page content',
      'delete any page content',
    ]);
  }

  /**
   * Tests deleting nodes.
   */
  public function testDeleteNodesAccess() {
    // Create ten nodes.
    $nodes = [];
    for ($i = 1; $i <= 10; $i++) {
      $nodes[$i] = $this->createNode([
        'type' => 'page',
        'title' => "Test Page $i",
        'status' => 1,
      ]);
    }

    // Set one as the front page.
    \Drupal::configFactory()->getEditable('system.site')
      ->set('page.front', '/node/' . $nodes[1]->id())
      ->save();
    // Set one as the 404 page.
    \Drupal::configFactory()->getEditable('system.site')
      ->set('page.404', '/node/' . $nodes[2]->id())
      ->save();
    // Set one as the 403 page.
    \Drupal::configFactory()->getEditable('system.site')
      ->set('page.403', '/node/' . $nodes[3]->id())
      ->save();
    // Set two as custom protected pages.
    \Drupal::configFactory()->getEditable('prevent_homepage_deletion.settings')
      ->set('protected_urls', '/node/' . $nodes[4]->id() . "\n/node/" . $nodes[5]->id())
      ->save();

    // User can not delete homepage.
    $this->assertFalse($nodes[1]->access('delete'));
    // User can not delete 404 page.
    $this->assertFalse($nodes[2]->access('delete'));
    // User can not delete 403 page.
    $this->assertFalse($nodes[3]->access('delete'));
    // User can not delete custom protected.
    $this->assertFalse($nodes[4]->access('delete'));
    $this->assertFalse($nodes[5]->access('delete'));
    // User can delete other pages.
    $this->assertTrue($nodes[7]->access('delete'));
    $this->assertTrue($nodes[9]->access('delete'));

    // User with permission to delete homepage.
    $this->setUpCurrentUser([], [
      'access content',
      'create page content',
      'edit any page content',
      'delete any page content',
      'delete_homepage_node',
    ]);

    // User can delete all pages.
    $this->assertTrue($nodes[1]->access('delete'));
    $this->assertTrue($nodes[2]->access('delete'));
    $this->assertTrue($nodes[3]->access('delete'));
    $this->assertTrue($nodes[4]->access('delete'));
    $this->assertTrue($nodes[5]->access('delete'));
    $this->assertTrue($nodes[7]->access('delete'));
    $this->assertTrue($nodes[9]->access('delete'));
  }

}
